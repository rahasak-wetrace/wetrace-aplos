package com.score.aplos.cassandra

import java.util.Date

import com.dataoperandz.cassper.Cassper
import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.{Await, Future}

case class Blob(id: String, blob: String, timestamp: Date = new Date())

trait BlobStore extends CassandraCluster {
  import ctx._

  def createBlobAsync(blob: Blob): Future[Unit] = {
    val q = quote {
      query[Blob].insert(lift(blob))
    }
    ctx.run(q)
  }

  def updateBlobAsync(blob: Blob): Future[Unit] = {
    val q = quote {
      query[Blob]
        .filter(p => p.id == lift(blob.id))
        .update(
          _.blob -> lift(blob.blob)
        )
    }
    ctx.run(q)
  }

  def searchBlobAsync(id: String): Future[List[Blob]] = {
    val q = quote {
      query[Blob]
        .filter(p => p.id == lift(id))
        .take(1)
    }
    ctx.run(q)
  }
}
