package com.score.aplos.cassandra

import java.util.Date

import com.dataoperandz.cassper.Cassper
import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.{Await, Future}

case class Vaccine(userMobile: String, userName: String, vaccineStatus: String, timestamp: Date = new Date())

trait VaccineStore extends CassandraCluster {
  import ctx._

  def createVaccineAsync(score: Vaccine): Future[Unit] = {
    val q = quote {
      query[Vaccine].insert(lift(score))
    }
    ctx.run(q)
  }

  def updateVaccineAsync(vaccine: Vaccine): Future[Unit] = {
    val q = quote {
      query[Vaccine]
        .filter(p => p.userMobile == lift(vaccine.userMobile))
        .update(
          _.userName -> lift(vaccine.userName),
          _.vaccineStatus -> lift(vaccine.vaccineStatus)
        )
    }
    ctx.run(q)
  }

  def getVaccineAsync(userMobile: String): Future[List[Vaccine]] = {
    val q = quote {
      query[Vaccine]
        .filter(p => p.userMobile == lift(userMobile))
        .take(1)
    }
    ctx.run(q)
  }
}
