package com.score.aplos.cassandra

import com.datastax.driver.core.{Cluster, _}
import io.getquill.context.cassandra.CassandraSessionContext

import scala.collection.JavaConverters._

trait UdtMapper {

  this: CassandraSessionContext[_] =>

  var cluster: Cluster
  var keySpace: String

  implicit val locationDecoder: Decoder[Location] = {
    decoder((index, row) => {
      val a = row.getUDTValue(index)
      Location(a.getDouble("lat"), a.getDouble("lon"))
    })
  }

  implicit val locationEncoder: Encoder[Location] = {
    encoder((index, location, row) => {
      val tpe = cluster.getMetadata.getKeyspace("rahasak").getUserType("geo_point")
      val udt = tpe.newValue()
        .setDouble("lat", location.lat)
        .setDouble("lon", location.lon)
      row.setUDTValue(index, udt)
    })
  }

}

