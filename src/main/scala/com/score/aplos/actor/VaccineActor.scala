package com.score.aplos.actor

import akka.actor.{Actor, Props}
import akka.stream.ActorMaterializer
import com.score.aplos.actor.VaccineActor._
import com.score.aplos.cassandra._
import com.score.aplos.config.{ApiConf, AppConf, FeatureToggleConf}
import com.score.aplos.elastic.ElasticStore
import com.score.aplos.elastic.ElasticStore.{Criteria, Page, Sort}
import com.score.aplos.protocol._
import com.score.aplos.util.{AppLogger, DateFactory}

import scala.util.Try

object VaccineActor {

  case class AddVaccine(messageType: String, execer: String, id: String,
                        userMobile: String, userName: String, vaccineStatus: String) extends VaccineMessage

  case class UpdateVaccine(messageType: String, execer: String, id: String,
                           userMobile: String) extends VaccineMessage

  case class GetVaccine(messageType: String, execer: String, id: String, userMobile: String) extends VaccineMessage

  case class SearchVaccine(messageType: String, execer: String, id: String, offset: String, limit: String,
                    userMobileTerm: String, userMobile: String, sort: String = "descending") extends VaccineMessage

  def props()(implicit materializer: ActorMaterializer) = Props(new VaccineActor())

}

class VaccineActor()(implicit materializer: ActorMaterializer) extends Actor with AppConf with ApiConf with FeatureToggleConf with AppLogger {

  override def receive: Receive = {
    case msg: AddVaccine =>
      logger.info(s"got AddVaccine message - $msg")

      val score = Vaccine(msg.userMobile, msg.userName, msg.vaccineStatus)
      CassandraStore.createVaccine(score)

      sender ! StatusReply(201, "vaccine added")

      context.stop(self)

    case msg: GetVaccine =>
      logger.info(s"got GetVaccine message - $msg")

      CassandraStore.getVaccine(msg.userMobile) match {
        case Some(s) =>
          logger.info(s"found vaccine $s")
          sender ! VaccineReply(s.userMobile, s.userName, s.vaccineStatus, DateFactory.formatToString(Option(s.timestamp), DateFactory.TIMESTAMP_FORMAT, DateFactory.COLOMBO_TIME_ZONE))
        case None =>
          logger.error(s"vaccine not found for ${msg.userMobile}")
          sender ! StatusReply(404, "not found")
      }

      context.stop(self)

    case search: SearchVaccine =>
      logger.info(s"got search message - $search")

      // wildcards
      var wildcards = List[Criteria]()
      if (!search.userMobileTerm.isEmpty) wildcards = wildcards :+ Criteria("user_mobile", search.userMobileTerm)

      // filters
      var criterias = List[Criteria]()
      criterias = criterias :+ Criteria("user_mobile", search.userMobile)

      // sorts and pages
      val sorts = List(Sort("timestamp", if (search.sort.equalsIgnoreCase("ascending")) true else false))
      val page = Page(Try(search.offset.toInt).getOrElse(0), Try(search.limit.toInt).getOrElse(10))

      val (hits, vaccines) = ElasticStore.getVaccines(List(), wildcards, criterias, List(), None, None, sorts, Option(page))
      val meta = VaccineMeta(search.offset, search.limit, vaccines.size.toString, hits.toString)
      val searchReply = VaccineSearchReply(meta, vaccines)

      import com.score.aplos.protocol.VaccineSearchReplyProtocol._
      import spray.json._
      logger.info(s"block reply json ${searchReply.toJson.toString()}")

      sender ! searchReply

      context.stop(self)
  }

}
