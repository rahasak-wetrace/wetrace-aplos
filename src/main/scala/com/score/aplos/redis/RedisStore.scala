package com.score.aplos.redis

import com.score.aplos.util.AppLogger

import scala.collection.JavaConverters._
import scala.collection.mutable

object RedisStore extends AppLogger with RedisCluster {

  def get: mutable.Set[String] = {
    val jedis = redisPool.getResource
    val ids = jedis.smembers(redisKey).asScala
    redisPool.returnResource(jedis)

    logger.info(s"get trans ids in redis $ids")

    ids
  }

  def set(id: String): Boolean = {
    val jedis = redisPool.getResource
    val i = jedis.sadd(redisKey, id)
    redisPool.returnResource(jedis)

    logger.info(s"add id - $id to redis, status - $i ")

    i == 1L
  }

  def rm(ids: List[String]): Long = {
    val jedis = redisPool.getResource
    val arr = ids.toArray
    val i = jedis.srem(redisKey, arr: _*)
    redisPool.returnResource(jedis)

    logger.info(s"remove trans ids redis $ids")

    i
  }

}

