package com.score.aplos.config

import com.typesafe.config.ConfigFactory

import scala.util.Try

trait SmsConf {

  val smsConf = ConfigFactory.load("sms.conf")

  lazy val smsApi = Try(smsConf.getString("sms.api")).getOrElse("https://cpsolutions.dialog.lk/index.php/cbs/sms/send")
  lazy val smsApiKey = Try(smsConf.getString("sms.api-key")).getOrElse("14767828958768")

}
