package com.score.aplos

import akka.actor.ActorSystem
import com.score.aplos.cassandra.CassandraStore
import com.score.aplos.config.{AppConf, FeatureToggleConf}
import com.score.aplos.elastic.ElasticStore
import com.score.aplos.http.HttpRouter
import com.score.aplos.util.{CryptoFactory, LoggerFactory}

object Main extends App with AppConf with FeatureToggleConf {

  // setup logging
  LoggerFactory.init()

  // actor systems
  implicit val system: ActorSystem = ActorSystem.create("rahasak")

  // set up keys
  CryptoFactory.init()

  // create schema/indexes
  CassandraStore.init()
  ElasticStore.init()

  // serve http server
  HttpRouter.serve()

}

