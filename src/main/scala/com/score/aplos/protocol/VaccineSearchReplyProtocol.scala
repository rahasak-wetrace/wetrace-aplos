package com.score.aplos.protocol

import spray.json.{DefaultJsonProtocol, _}

case class VaccineMeta(offset: String, limit: String, count: String, total: String)

case class VaccineSearchReply(meta: VaccineMeta, accounts: List[VaccineReply])

object VaccineSearchReplyProtocol extends DefaultJsonProtocol {
  implicit val metaFormat: JsonFormat[VaccineMeta] = jsonFormat4(VaccineMeta)

  import com.score.aplos.protocol.VaccineReplyProtocol._

  implicit object VaccineSearchReplyFormat extends RootJsonFormat[VaccineSearchReply] {
    override def write(obj: VaccineSearchReply): JsValue = {
      JsObject(
        ("meta", obj.meta.toJson),
        ("vaccines", obj.accounts.toJson)
      )
    }

    override def read(json: JsValue) = ???
  }

}



