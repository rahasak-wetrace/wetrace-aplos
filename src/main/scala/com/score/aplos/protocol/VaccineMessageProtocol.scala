package com.score.aplos.protocol

import com.score.aplos.actor.VaccineActor._
import spray.json._

trait VaccineMessage

object VaccineMessageProtocol extends DefaultJsonProtocol {

  implicit val format1: JsonFormat[AddVaccine] = jsonFormat6(AddVaccine)
  implicit val format2: JsonFormat[GetVaccine] = jsonFormat4(GetVaccine)
  implicit val format3: JsonFormat[SearchVaccine] = jsonFormat8(SearchVaccine)

  implicit object VaccineMessageFormat extends RootJsonFormat[VaccineMessage] {
    def write(obj: VaccineMessage): JsValue =
      JsObject((obj match {
        case p: AddVaccine => p.toJson
        case p: GetVaccine => p.toJson
        case p: SearchVaccine => p.toJson
        case unknown => deserializationError(s"json deserialize error: $unknown")
      }).asJsObject.fields)

    def read(json: JsValue): VaccineMessage =
      json.asJsObject.getFields("messageType") match {
        case Seq(JsString("addVaccine")) => json.convertTo[AddVaccine]
        case Seq(JsString("getVaccine")) => json.convertTo[GetVaccine]
        case Seq(JsString("searchVaccine")) => json.convertTo[SearchVaccine]
        case unrecognized => serializationError(s"json serialization error $unrecognized")
      }
  }

}

//object M extends App {
//
//  import WatchMessageProtocol._
//
//  val i = Create("create", "eraga", "23121", "111", "tisslot", "black", "eranga")
//  val s = i.toJson.toString
//  println(s)
//  s.parseJson.convertTo[WatchMessage] match {
//    case i: Create => println(s"init $i")
//    case p: Put => println(s"put $p")
//    case g: Get => println(s"get $g")
//  }
//
//}


