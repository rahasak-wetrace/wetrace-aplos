package com.score.aplos.protocol

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol

case class VaccineReply(userMobile: String,
                        userName: String,
                        vaccineStatus: String,
                        timestamp: Option[String]
                     )

object VaccineReplyProtocol extends SprayJsonSupport with DefaultJsonProtocol {

  implicit val format = jsonFormat4(VaccineReply)

}

